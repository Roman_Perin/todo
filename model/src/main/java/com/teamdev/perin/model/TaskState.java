package com.teamdev.perin.model;

public enum TaskState {
    ACTIVE,
    EXECUTED,
}
