package com.teamdev.perin.model.services;

public class Validator {


    public static void checkEmail(String email) throws InvalidArgumentException {
        String blankLine = "";
        String regexp = ".+@.+\\..+";
        if ((email == null) || email.equals(blankLine)) throw new InvalidArgumentException("Email doesn't specified.");
        if (!email.matches(regexp)) throw new InvalidArgumentException("Invalid email address, please, check it.");
    }

    public static void checkPassword(String password) throws InvalidArgumentException {
        int minimalPasswordLength = 1;
        checkOnNull("Password doesn't specified.", password);
        if (password.length() < minimalPasswordLength) {
            throw new InvalidArgumentException("Password is shorter than valid minimal " +
                                               "length:\"" + minimalPasswordLength + "\".");
        }
    }

    public static void checkTextOfTask(String text) throws InvalidArgumentException {
        String blankLine = "";
        if ((text == null) || text.equals(blankLine)) throw new InvalidArgumentException("Text doesn't specified.");
    }

    public static void checkOnNull(String message, Object checkingObject) throws InvalidArgumentException {
        if (checkingObject == null){
            throw new InvalidArgumentException(message);
        }
    }

}
