package com.teamdev.perin.model.dao;

import com.teamdev.perin.model.Task;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TaskManager extends CrudRepository<Task, Long>{


//    @Query(value = "SELECT t FROM Task t where t.assignee.id=:assigneesId order by t.date desc")
//    List<Task> findByAssigneeId(@Param("assigneesId") long assigneesId);
//    @Transactional(isolation = Isolation.SERIALIZABLE)
//    @org.hibernate.annotations.Cache(usage= CacheConcurrencyStrategy.NONE)
    List<Task> findByAssignee_Id(long assigneesId);

//    @Transactional(isolation = Isolation.SERIALIZABLE)
//    @org.hibernate.annotations.Cache(usage= CacheConcurrencyStrategy.NONE)
    List<Task> findByCreator_IdAndAssignee_IdNotLike(long creatorsId, long assigneesId);

    Task findById(Long id);

}
