package com.teamdev.perin.model;


import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@org.hibernate.annotations.Cache(usage= CacheConcurrencyStrategy.NONE)
public class Task implements Comparable<Task>, Serializable{

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    @Lob
    private String text;
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfCreation;
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
    private User creator;
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
    private User assignee;
    @NotNull
    @Enumerated
    private TaskState state;


    public Task() {
    }

    public Task(String text, User creator, User assignee) {
        this.text = text;
        this.creator = creator;
        this.assignee = assignee;
    }


    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }
    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public TaskState getState() {
        return state;
    }
    public void setState(TaskState state) {
        this.state = state;
    }

    public User getCreator() {
        return creator;
    }
    public void setCreator(User creator) {
        this.creator = creator;
    }

    public User getAssignee() {
        return assignee;
    }
    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }


    @Override
    public int compareTo(Task other) {
        int timeDifference = Long.compare(this.getDateOfCreation().getTime(), other.getDateOfCreation().getTime());
        if (timeDifference != 0){
            return timeDifference;
        }
        return  Long.compare(this.id, other.getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task other = (Task) o;

        return this.id == other.getId();
    }

    @Override
    public int hashCode() {
        return (int) (this.id ^ (this.id >>> 32));
    }


    @Override
    public String toString() {
        return "TaskDto{" +
                "id=" + id +
                ", tasksText=" + text +
                ", dateOfCreation=" + dateOfCreation +
                ", creatorsId=" + creator.getId() +
                ", assigneesId=" + assignee.getId() +
                ", tasksState=" + state +
                '}';
    }

}
