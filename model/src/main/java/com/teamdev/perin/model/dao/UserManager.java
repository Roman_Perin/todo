package com.teamdev.perin.model.dao;

import com.teamdev.perin.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserManager extends CrudRepository<User, Long> {


    User findByLogin(String login);

    User findById(Long id);

}
