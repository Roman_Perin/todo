package com.teamdev.perin.model.dto;

import com.teamdev.perin.model.User;

public class UserDto {

    private long id;
    private String login;


    public UserDto() {
    }

    public UserDto(long id, String login) {
        this.id = id;
        this.login = login;
    }

    public UserDto(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
    }


    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", login=" + login +
                '}';
    }
}
