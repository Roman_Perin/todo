package com.teamdev.perin.model.services;

import com.teamdev.perin.model.Task;
import com.teamdev.perin.model.TaskState;
import com.teamdev.perin.model.User;
import com.teamdev.perin.model.dao.TaskManager;
import com.teamdev.perin.model.dao.UserManager;
import com.teamdev.perin.model.dto.TaskDto;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


@Service
public class TaskService {

    private static Logger logger = LoggerFactory.getLogger(TaskService.class);

    @Autowired
    private TaskManager taskManager;
    @Autowired
    private UserManager userManager;


    public TaskDto createTask(TaskDto taskDto) throws InvalidArgumentException{
        if (logger.isTraceEnabled()){
            logger.trace("New task is creating on basis of taskDto:{}.", taskDto);
        }
        //todo: what to do with validators?
        Validator.checkTextOfTask(taskDto.getTasksText());

        User creator = userManager.findById(taskDto.getCreatorsId());
        User assignee = userManager.findById(taskDto.getAssigneesId());
        Date currentTime = new Date();

        Task newTask = new Task(taskDto.getTasksText(), creator, assignee);
        newTask.setDateOfCreation(currentTime);
        newTask.setState(TaskState.ACTIVE);

        Task storedTask = taskManager.save(newTask);

        return new TaskDto(storedTask);
    }


     public boolean updateTask(TaskDto taskDto) throws InvalidArgumentException{
        if (logger.isTraceEnabled()){
            logger.trace("Task is updating on basis of taskDto:{}.", taskDto);
        }
        Validator.checkOnNull("State must to be initialized.", taskDto.getTasksState());
        Validator.checkTextOfTask(taskDto.getTasksText());

        User newAssignee = userManager.findById(taskDto.getAssigneesId());
        Task updatingTask = taskManager.findById(taskDto.getId());

        updatingTask.setState(taskDto.getTasksState());
        updatingTask.setText(taskDto.getTasksText());
        updatingTask.setAssignee(newAssignee);

        //todo: always true? - Curly logic... I have to think how to change it
        return true;
    }



    public void deleteTaskById(long id){
        if (logger.isTraceEnabled()){
            logger.trace("Task with id:{} is deleting.", id);
        }
        taskManager.delete(id);
    }


    public List<TaskDto> findTasksDelegatedByUserToOthers(long creatorsId){
        if (logger.isTraceEnabled()){
            logger.trace("Tasks delegated by user with id:{} are searching.", creatorsId);
        }
        List<Task> tasksForExecution = taskManager.findByCreator_IdAndAssignee_IdNotLike(creatorsId, creatorsId);
        return convertFromTaskToTaskDto(tasksForExecution);
    }


     public List<TaskDto> findTasksDelegatedToUser(long assigneesId){
        if (logger.isTraceEnabled()){
            logger.trace("Tasks delegated to user with id:{} are searching.", assigneesId);
        }
        List<Task> delegatedTasks = taskManager.findByAssignee_Id(assigneesId);
        return convertFromTaskToTaskDto(delegatedTasks);
    }


    private List<TaskDto> convertFromTaskToTaskDto(Collection<Task> tasks){
        List<TaskDto> output = new ArrayList<TaskDto>();
        for (Task task : tasks) {
            output.add(new TaskDto(task));
        }
        return output;
    }

}
