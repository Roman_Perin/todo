package com.teamdev.perin.model;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

@Entity
@org.hibernate.annotations.Cache(usage= CacheConcurrencyStrategy.NONE)
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Email
    @NotNull
    @Column(unique = true)
    private String login;

    @NotNull
    @NotEmpty
    private String password;

    @OneToMany(mappedBy = "assignee", fetch = FetchType.EAGER)
    private Set<Task> tasksForCurrentUser = new TreeSet<Task>();

    @OneToMany(mappedBy = "creator", fetch = FetchType.EAGER)
    private Set<Task> delegatedTasks = new TreeSet<Task>();


    public User() {
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }


    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Task> getTasksForCurrentUser() {
        return tasksForCurrentUser;
    }
    public void setTasksForCurrentUser(Set<Task> tasksForCurrentUser) {
        this.tasksForCurrentUser = tasksForCurrentUser;
    }

    public Set<Task> getDelegatedTasks() {
        return delegatedTasks;
    }
    public void setDelegatedTasks(Set<Task> delegatedTasks) {
        this.delegatedTasks = delegatedTasks;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User other = (User) o;

        return this.id == other.getId();
    }

    @Override
    public int hashCode() {
        return (int) (this.id ^ (this.id >>> 32));
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", login=" + login +
                '}';
    }
}
