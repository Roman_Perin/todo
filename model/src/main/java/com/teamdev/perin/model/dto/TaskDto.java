package com.teamdev.perin.model.dto;

import com.teamdev.perin.model.Task;
import com.teamdev.perin.model.TaskState;

import java.util.Date;

public class TaskDto {

    private long id;
    private String tasksText;
    private Date dateOfCreation;
    private long creatorsId;
    private long assigneesId;
    private TaskState tasksState;


    public TaskDto() {
    }

    public TaskDto(String tasksText, long creatorsId, long assigneesId) {
        this.tasksText = tasksText;
        this.creatorsId = creatorsId;
        this.assigneesId = assigneesId;
    }

    public TaskDto(long id, String tasksText, long assigneesId, TaskState tasksState) {
        this.id = id;
        this.tasksText = tasksText;
        this.assigneesId = assigneesId;
        this.tasksState = tasksState;
    }

    public TaskDto(Task task) {
        this.id = task.getId();
        this.tasksText = task.getText();
        this.dateOfCreation = task.getDateOfCreation();
        this.tasksState = task.getState();
        this.creatorsId = task.getCreator().getId();
        this.assigneesId = task.getAssignee().getId();
    }


    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getTasksText() {
        return tasksText;
    }
    public void setTasksText(String tasksText) {
        this.tasksText = tasksText;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }
    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public TaskState getTasksState() {
        return tasksState;
    }
    public void setTasksState(TaskState tasksState) {
        this.tasksState = tasksState;
    }

    public long getCreatorsId() {
        return creatorsId;
    }
    public void setCreatorsId(long creatorsId) {
        this.creatorsId = creatorsId;
    }

    public long getAssigneesId() {
        return assigneesId;
    }
    public void setAssigneesId(long assigneesId) {
        this.assigneesId = assigneesId;
    }

    @Override
    public String toString() {
        return "TaskDto{" +
                "id=" + id +
                ", tasksText=" + tasksText +
                ", dateOfCreation=" + dateOfCreation +
                ", creatorsId=" + creatorsId +
                ", assigneesId=" + assigneesId +
                ", tasksState=" + tasksState +
                '}';
    }
}
