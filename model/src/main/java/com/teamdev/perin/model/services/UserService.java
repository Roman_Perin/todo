package com.teamdev.perin.model.services;

import com.teamdev.perin.model.User;
import com.teamdev.perin.model.dao.UserManager;
import com.teamdev.perin.model.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserService {

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserManager userManager;


    public UserDto createUser(String login, String password){
        User newUser = new User(login, password);
        userManager.save(newUser);
        return getUserByLogin(login);
    }

    public UserDto getUserById(long id){
        return new UserDto(userManager.findById(id));
    }


    public UserDto getUserByLogin(String login){
        return new UserDto(userManager.findByLogin(login));
    }


    public List<UserDto> getSortedUsers(){
        Iterable<User> users = userManager.findAll();
        List<UserDto> userDtos = new ArrayList<UserDto>();
        for (User user: users){
            userDtos.add(new UserDto(user));
        }
        return userDtos;
    }

}
