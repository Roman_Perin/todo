package com.teamdev.perin.todolist.config;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(classes = SpringConfig.class)
public class AbstractTest extends AbstractJUnit4SpringContextTests{
}
