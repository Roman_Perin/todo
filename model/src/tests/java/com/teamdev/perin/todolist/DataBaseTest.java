package com.teamdev.perin.todolist;


import com.teamdev.perin.model.TaskState;
import com.teamdev.perin.model.dto.TaskDto;
import com.teamdev.perin.model.services.InvalidArgumentException;
import com.teamdev.perin.model.services.TaskService;
import com.teamdev.perin.model.services.UserService;
import com.teamdev.perin.todolist.config.AbstractTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

public class DataBaseTest extends AbstractTest {

    @Autowired
    private TaskService taskService;

    @Autowired
    private UserService userService;


    private String  newUserLogin = "testUser@email.com";
    private String  password = "password";
    private String  text = "JUnit text for new task.";
    private Date    dateOfCreation = new Date();
    private long    creatorsId = 3;
    private long    assigneesId = 3;
    private long    newUsersId = 3;
    private TaskState  state = TaskState.ACTIVE;


    @Before
    public void init(){

    }

    @After
    public void drop(){
//        List<TaskDto> delegatedTasks = taskService.findTasksDelegatedByUserToOthers(newUsersId);
//        List<TaskDto> tasksForUser = taskService.findTasksDelegatedToUser(newUsersId);
//        for (TaskDto delegatedTask: delegatedTasks) {
//            taskService.deleteTaskById(delegatedTask.getId());
//        }
//        for (TaskDto taskForUser: tasksForUser) {
//            taskService.deleteTaskById(taskForUser.getId());
//        }
    }


    @Test
    public void createTask(){
        TaskDto taskDto1 = new TaskDto(text + 1, creatorsId, assigneesId);
        TaskDto taskDto2 = new TaskDto(text + 2, creatorsId, assigneesId);
        TaskDto taskDto3 = new TaskDto(text + 3, creatorsId, assigneesId);

        try {
            System.out.println("creating taskDto1");
            taskService.createTask(taskDto1);
            Assert.assertEquals("sdss", 1, taskService.findTasksDelegatedToUser(newUsersId).size());

            System.out.println("creating taskDto2");
            taskService.createTask(taskDto2);
            Assert.assertEquals("sdss", 2, taskService.findTasksDelegatedToUser(newUsersId).size());

            System.out.println("creating taskDto3");
            taskService.createTask(taskDto3);
            Assert.assertEquals("sdss", 3, taskService.findTasksDelegatedToUser(newUsersId).size());
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void createNewTasks(){
        TaskDto taskDto4 = new TaskDto(text + 4, creatorsId, assigneesId);
        TaskDto taskDto5 = new TaskDto(text + 5, creatorsId, assigneesId);

        try {
            userService.getUserById(3);
            System.out.println("creating taskDto4");
            taskService.createTask(taskDto4);
            List<TaskDto> tasksDelegatedToUser4 = taskService.findTasksDelegatedToUser(3);
            for (int i = 0; i < 3; i++){
                Thread t1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        userService.getUserById(3);
                    }
                });
                Thread t2 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        userService.getUserById(3);
                    }
                });
                t1.start();
                t2.start();
            }
            Assert.assertEquals("New task wasn't returned from the server.", 4, tasksDelegatedToUser4.size());

            userService.getUserById(3);
            System.out.println("creating taskDto5");
            taskService.createTask(taskDto5);
            List<TaskDto> tasksDelegatedToUser5 = taskService.findTasksDelegatedToUser(3);
            for (int i = 0; i < 3; i++){
                Thread t1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        userService.getUserById(3);
                    }
                });
                Thread t2 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        userService.getUserById(3);
                    }
                });
                t1.start();
                t2.start();
            }
            Assert.assertEquals("New task wasn't returned from the server.", 5, tasksDelegatedToUser5.size());
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        }




    }


}
