package com.teamdev.perin.todolist.config;

import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Configuration
@ComponentScan("com.teamdev.perin.model")
@RunWith(SpringJUnit4ClassRunner.class)
@ImportResource({"META-INF/persistence.cfg.xml"})
@EnableJpaRepositories("com.teamdev.perin.model.dao")
public class SpringConfig {
}
