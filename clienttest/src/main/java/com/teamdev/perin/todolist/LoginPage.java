package com.teamdev.perin.todolist;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;


public class LoginPage {

    private WebDriver driver = DriverManager.getDriver();

    public LoginPage(String url) {
        driver.get(url);
    }

    public static final By LOGIN_DIV = By.id("login");
    public static final By LOGIN_INPUT = By.id("txt-email");
    public static final By PASSWORD_INPUT = By.id("password");
    public static final By SIGNIN_BUTTON = By.cssSelector(".submit>input");


    public ToDoListPage login(String login, String password){
        driver.findElement(LOGIN_INPUT).sendKeys(login);
        driver.findElement(PASSWORD_INPUT).sendKeys(password);
        driver.findElement(SIGNIN_BUTTON).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        return new ToDoListPage(driver);
    }


    public boolean isDisplayed(){
        return driver.findElement(LOGIN_DIV).isDisplayed();
    }


    public void close(){
        driver.close();
    }
}
