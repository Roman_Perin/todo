package com.teamdev.perin.todolist;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ToDoListPage {

    public static final By TODOLIST1 = By.id("todolist1");
    public static final By TODOLIST2 = By.id("todolist2");
    public static final By NEW_TASK_TEXT_AREA = By.cssSelector("#todolist1 .text-for-new-task");
    public static final By LAST_TASK_LABEL = By.xpath("(//*[@id='todolist1']//*[@class='lbl-text'])[1]");
    public static final By LAST_TASK_DIV = By.xpath("(//*[@id='todolist1']//*[@class='task'])[1]");
    public static final By ADD_TASK_BUTTON = By.cssSelector("#todolist1 .tbn-task-creator");
    public static final By DELETE_LAST_TASK_BUTTON = By.xpath("(//*[@id='todolist1']//button[2])[1]");

    private WebDriver driver;

    public ToDoListPage(WebDriver driver) {
        this.driver = driver;
    }


    public boolean areListsDisplayed(){
       if (driver.findElement(TODOLIST1).isDisplayed() && driver.findElement(TODOLIST2).isDisplayed()){
           return true;
       } else {
           return false;
       }
    }


    public ToDoListPage createTask(String taskText){
        waitElementVisibility(NEW_TASK_TEXT_AREA);
        driver.findElement(NEW_TASK_TEXT_AREA).click();
        driver.findElement(NEW_TASK_TEXT_AREA).sendKeys(taskText);
        driver.findElement(ADD_TASK_BUTTON).click();
        return this;
    }


    public String readLastTask(){
        try {
            Thread.currentThread().sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        new WebDriverWait(driver, 5).until(ExpectedConditions.presenceOfElementLocated(LAST_TASK_LABEL));
        return driver.findElement(LAST_TASK_LABEL).getText();
    }


    public String readIdOfLastTask(){
        return driver.findElement(LAST_TASK_DIV).getAttribute("id");
    }


    public void deleteLastTaskWithWaitingForBecomingInvisible(){
        String lastTaskId = readIdOfLastTask();
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(LAST_TASK_DIV));
        //todo: will it works if remove next line
        driver.findElement(LAST_TASK_DIV).click();
        driver.findElement(DELETE_LAST_TASK_BUTTON).click();
        acceptAlert();
        waitForElementsInvisibility(By.id(lastTaskId));
    }


    public boolean checkElementPresentsById(String id){
        try {
            driver.findElement(By.id(id));
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    public void close(){
        driver.close();
    }


    private void waitElementVisibility(By searchMode){
        new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(searchMode));
    }


    private void waitForElementsInvisibility(By searchMode){
        new WebDriverWait(driver, 5).until(ExpectedConditions.invisibilityOfElementLocated(searchMode));
    }


    private void acceptAlert() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.alertIsPresent());
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

}