package com.teamdev.perin.todolist;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverManager {

    public static WebDriver getDriver(){
        WebDriver driver = new FirefoxDriver();
//        driver.manage().window().maximize();
        return driver;
    }

}
