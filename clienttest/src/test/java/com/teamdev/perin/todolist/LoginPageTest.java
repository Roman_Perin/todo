package com.teamdev.perin.todolist;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;


public class LoginPageTest {

    public static final String LOGIN_PAGE_URL = "http://localhost:8080/login";
    public static final String LOGIN = "user1";
    public static final String PASSWORD = "111";

    private LoginPage loginPage;

    @Before
    public void setLoginPage(){
        loginPage = new LoginPage(LOGIN_PAGE_URL);
    }

    @After
    public void closeLoginPage(){
        loginPage.close();
    }


    @Test
    public void findLoginPage() {
        assertTrue("At the root page have to be login form for unloggedin user.",
                   loginPage.isDisplayed());
    }

    @Test
    public void authorization() {
        ToDoListPage toDoListPage = loginPage.login(LOGIN, PASSWORD);
        assertTrue("After authorization user have to see his todo-lists but it wasn't happen.",
                   toDoListPage.areListsDisplayed());
    }

}