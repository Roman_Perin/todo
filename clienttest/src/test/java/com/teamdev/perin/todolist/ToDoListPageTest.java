package com.teamdev.perin.todolist;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class ToDoListPageTest {

    public static final String LOGIN_PAGE_URL = "http://localhost:8080/login";
    public static final String LOGIN = "user1";
    public static final String PASSWORD = "111";
    public static final String TEXT_FOR_NEW_TASK = "New task created by selenium.";

    private ToDoListPage toDoListPage;

    @Before
    public void setToDoListPage(){
        LoginPage loginPage = new LoginPage(LOGIN_PAGE_URL);
        toDoListPage = loginPage.login(LOGIN, PASSWORD);
    }

    @After
    public void closeToDoListPage(){
        toDoListPage.close();
    }

    @Test
    public void addNewTask(){
        createTask(TEXT_FOR_NEW_TASK);
        assertEquals("New task have to on the top of the list.",
                     TEXT_FOR_NEW_TASK, toDoListPage.readLastTask());
    }

    @Test
    public void deleteTask(){
        createTask(TEXT_FOR_NEW_TASK);
        assertEquals("Task for deletion wasn't create or was positioned not on the top of the list.",
                     TEXT_FOR_NEW_TASK, toDoListPage.readLastTask());
        String deletingTaskId = toDoListPage.readIdOfLastTask();
        toDoListPage.deleteLastTaskWithWaitingForBecomingInvisible();
        assertFalse("Last task wasn't deleted.", toDoListPage.checkElementPresentsById(deletingTaskId));
    }

    private void createTask(String taskText){
        toDoListPage.createTask(taskText);
    }
}
