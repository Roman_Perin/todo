package com.teamdev.perin.controllers;

import com.teamdev.perin.model.dto.UserDto;
import com.teamdev.perin.model.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Collection;


@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping("/{id}")
    public UserDto getUserById(@PathVariable long id){
        return userService.getUserById(id);
    }

    @RequestMapping("/login/{login}")
    public UserDto getUserByLogin(@PathVariable String login){
        return userService.getUserByLogin(login);
    }


    @RequestMapping("")
    public Collection<UserDto> getAllUsers(){
        return userService.getSortedUsers();
    }


    @RequestMapping("/current")
    public UserDto getCurrentUsers(Principal principal){
        String login = principal.getName();
        return userService.getUserByLogin(login);
    }

}
