package com.teamdev.perin.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamdev.perin.model.dto.TaskDto;
import com.teamdev.perin.model.services.InvalidArgumentException;
import com.teamdev.perin.model.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;


    @RequestMapping("/delegatedtouser/{id}")
    public List<TaskDto> getTasksDelegatedToUser(@PathVariable long id){
//        taskService.findTasksDelegatedToUser(id);
        List<TaskDto> tasks = taskService.findTasksDelegatedToUser(id);
System.err.println("To " + tasks.size());
        return tasks;
    }


    @RequestMapping("/delegatedbyuser/{id}")
    public List<TaskDto> getTasksDelegatedByUserToOthers(@PathVariable long id){
        List<TaskDto> tasks = taskService.findTasksDelegatedByUserToOthers(id);
System.err.println("delegated BY " + tasks.size());
        return tasks;
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public TaskDto createTask(@RequestBody String taskJson){
        ObjectMapper mapper = new ObjectMapper();
        TaskDto taskDto = null;
        try {
            taskDto = mapper.readValue(taskJson, TaskDto.class);
            taskService.createTask(taskDto);
            //todo: To catch exceptions correctly.
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        }
        return taskDto;
    }


    @RequestMapping(value = "{id}/update", method = RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
    public TaskDto updateTask(@RequestBody String taskJson, @PathVariable long id){
        ObjectMapper mapper = new ObjectMapper();
        TaskDto taskDto = null;
        try {
            taskDto = mapper.readValue(taskJson, TaskDto.class);
            taskService.updateTask(taskDto);
            //todo: To catch exceptions correctly.
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        }
        return taskDto;
    }


    @RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
    public long deleteTask(@PathVariable long id){
        taskService.deleteTaskById((long)id);
        //todo: May be it is not the best variant to return id...
        return id;
    }

}
