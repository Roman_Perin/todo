package com.teamdev.perin.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageController {

    @RequestMapping("/index")
    public String getHomePage(){
        return "resources/index.html";
    }


    @RequestMapping(value = {"/", "/login"})
    public String redirectLogin() {
        return "resources/views/login.html";
    }
}
