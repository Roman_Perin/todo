package com.teamdev.perin.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;
  
public class WebInitializer implements WebApplicationInitializer {


	public void onStartup(ServletContext container) throws ServletException {

        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();  
        ctx.register(Config.class);
        ctx.setServletContext(container);

        container.addListener(new ContextLoaderListener(ctx));

        DispatcherServlet dispatcherServlet = new DispatcherServlet(ctx);
        Dynamic registration = container.addServlet("dispatcher", dispatcherServlet);
        registration.addMapping("/");

        registration.setLoadOnStartup(1);
    }  

}
