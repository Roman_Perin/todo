function ToDoListController(eventBus, title){

    var _self = this;
    this.taskListCreatorView = new TaskListCreatorView(eventBus, title);

    //Handling Events from task views.
    eventBus.on(Events.ADD_TASK, function(event, newTaskHolder){
        if (newTaskHolder.tasksText && !_isBlankText(newTaskHolder.tasksText)) {
            eventBus.trigger(Events.ADD_TASK_CONTROLLER_EVENT, newTaskHolder);
        }
    });

    eventBus.on(Events.CHANGE_STATUS, function(event, tasksAttributes){
        eventBus.trigger(Events.CHANGE_STATUS_CONTROLLER_EVENT, tasksAttributes);
    });

    eventBus.on(Events.CHANGE_TEXT, function(event, tasksAttributes){
        if (tasksAttributes.tasksText && !_isBlankText(tasksAttributes.tasksText)) {
            eventBus.trigger(Events.CHANGE_TEXT_CONTROLLER_EVENT, tasksAttributes);
        }
    });

    eventBus.on(Events.DELETE_TASK, function(event, initiatorsId){
        eventBus.trigger(Events.DELETE_TASK_CONTROLLER_EVENT, initiatorsId);
    });


    //Handling Events from TaskService.
    eventBus.on(Events.UPDATE_TASK_MODEL_EVENT, function (event, tasksHolder) {
        _self.taskListCreatorView.render(tasksHolder.tasks, tasksHolder.currentUser);
    });


    var _isBlankText = function(text){
        return /^\s*$/.test(text);
    };

}