var Events = {
    'ADD_TASK_LIST': 'ADD_TASK_LIST',
    'ADD_TASK': 'add_tasks',
    'ADD_TASK_CONTROLLER_EVENT': 'add_tasks_controller_event',
    'ADD_TASK_MODEL_EVENT': 'add_tasks_model_event',
    'CHANGE_STATUS': 'change_status',
    'CHANGE_STATUS_CONTROLLER_EVENT': 'change_status_controller_events',
    'UPDATE_TASK_MODEL_EVENT': 'update_task_model_events',
    'DELETE_TASK': 'delete_task',
    'DELETE_TASK_CONTROLLER_EVENT': 'delete_task_controller_events',
    'DELETE_TASK_MODEL_EVENT': 'delete_task_model_events',
    'CHANGE_TEXT': 'change_text',
    'CHANGE_TEXT_CONTROLLER_EVENT': 'task_change_controller_event'
}