function TaskListCreatorView(eventBus, title){

    var _self = this;

    var _taskCreatorView = $(".list-template").find(".task-list").clone().show();
    _taskCreatorView.appendTo(eventBus);
    var _newTaskTextarea = eventBus.find(".text-for-new-task");
    var _newTaskButton = eventBus.find(".tbn-task-creator");
    var _emptyLabel = eventBus.find(".lbl-empty-identifier");
    var _tasksHolder  = eventBus.find(".task-holder");
    var _assigneesLoginInput = eventBus.find(".inp-assignees-login");
    var _taskCreatorTitle = eventBus.find(".task-creator-title");
    var _disabledProperty = 'disabled';
    var _blankText = '';
    var _inputEvent = "input";

    _taskCreatorTitle.html(title);


    _self.render = function(tasks, currentUser){
        _tasksHolder.empty();
        _addTasks(tasks, currentUser);
        _setDisplayOptionForEmptyIdentifier(tasks);
    };


    //event handlers:
    _newTaskTextarea.on(_inputEvent, function(){
        var _tasksText = _newTaskTextarea.val();
        if (_isBlankText(_tasksText)){
            _newTaskButton.prop(_disabledProperty, true);
        } else {
            _newTaskButton.prop(_disabledProperty, false);
        }
    });


    _newTaskButton.click(function(){
        var _tasksText = _newTaskTextarea.val();
        _newTaskTextarea.val(_blankText);
        var _assigneesLogin = _assigneesLoginInput.val();
        _assigneesLoginInput.val(_blankText);
        _newTaskButton.prop(_disabledProperty, true);
        eventBus.trigger(Events.ADD_TASK, {tasksText: _tasksText, assigneesLogin: _assigneesLogin});
    });


    var _addTasks = function(tasks, currentUser) {
        addTaskByState(tasks, States.ACTIVE, currentUser);
        addTaskByState(tasks, States.EXECUTED, currentUser);
    };


    var addTaskByState = function(tasks, state, currentUser){
        for (var  key in tasks) {
            if (tasks[key] != undefined && tasks[key].tasksState === state) {
                new TaskView(tasks[key], _tasksHolder, currentUser);
            }
        }
    };


    var _setDisplayOptionForEmptyIdentifier = function(tasks){
        if (tasks.length) {
            _emptyLabel.hide();
        } else {
            _emptyLabel.show();
        }
    };


    var _isBlankText = function(text){
        return /^\s*$/.test(text);
    };

}