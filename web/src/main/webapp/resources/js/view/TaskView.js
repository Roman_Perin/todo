function TaskView(task, tasksHolder, currentUser){
    var _self = this;
    var _formatDate = function(date) {
        return date.getFullYear() + ":" + (date.getMonth() + 1) + ":" + date.getDate() + "-"
            + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    };

    _self.id = task.id;
    _self.dateOfCreation = task.dateOfCreation;
    _self.tasksState = task.tasksState;

    var _tasksText = task.tasksText;
    var _creator = task.creator;
    var _assignee = task.assignee;

    var data = {
        id: _self.id,
        creatorsLogin: _creator.login,
        tasksText: _tasksText,
        dateOfCreation: _formatDate(_self.dateOfCreation)
    }

    var source = $(".templates").html();
    var template = Handlebars.compile(source);

    tasksHolder.append(template(data));

    var _taskView = tasksHolder.find("#" + _self.id);
    var _saveButton = _taskView.find(".btn-save");
    var _textLabel = _taskView.find(".lbl-text");
    var _textarea = _taskView.find(".task-text");
    var _doUndoButton = _taskView.find(".btn-do-undo");
    var _deleteButton = _taskView.find(".btn-delete");
    var _otherUserSpan = _taskView.find(".spn-other-user");
    var _otherUserAssociationSpan = _taskView.find(".spn-other-user-association");
    var _tempText = _tasksText;

    var _renderingTime = 10;
    var _confirmDeletionMessage = "Are you really want to delete this task?"
    var _disabledProperty = 'disabled';
    var _textDecorationCss = 'text-decoration';
    var _crossedOutProperty = 'line-through';
    var _undoMark = "undo";
    var _inputEvent = "input";
    var _associatedToMarker = "To: ";
    var _associatedFromMarker = "From: ";

    _saveButton.hide();
    _doUndoButton.hide();
    _deleteButton.hide();
    _textarea.hide();


    if (_creator.id !== _assignee.id){
        if (currentUser.id === _creator.id){
            _otherUserAssociationSpan.text(_associatedToMarker);
            _otherUserSpan.text(_assignee.login);
        } else {
            _otherUserAssociationSpan.text(_associatedFromMarker);
            _otherUserSpan.text(_creator.login);
        }
    } else {
        _otherUserAssociationSpan.hide();
        _otherUserSpan.hide();
    }


    if (_self.tasksState === States.EXECUTED) {
        _textarea.css(_textDecorationCss, _crossedOutProperty);
        _textLabel.css(_textDecorationCss, _crossedOutProperty);
        _doUndoButton.text(_undoMark);
    }


    _taskView.mouseenter(function() {
        _saveButton.prop(_disabledProperty, true);
        _doUndoButton.show(_renderingTime);
        if (_creator.id === currentUser.id) {
            if (_self.tasksState === States.ACTIVE){
                _textLabel.hide(_renderingTime);
                _textarea.show(_renderingTime);
                _saveButton.show(_renderingTime);
            }
            _deleteButton.show(_renderingTime);
        }
    });


    _taskView.mouseleave(function() {
            _doUndoButton.hide(_renderingTime);
            _deleteButton.hide(_renderingTime);
            _saveButton.hide(_renderingTime);
            _textarea.hide();
            _textLabel.show();
            _textarea.val(_tempText);
    });


    _saveButton.click(function() {
        var _tasksText = _textarea.val();
        _tempText = _tasksText;
        _taskView.trigger(Events.CHANGE_TEXT, _getTasksAttributes());
    });


    _textarea.on(_inputEvent, function() {
        var _tasksText = _textarea.val();
        if (_isBlankText(_tasksText)){
            _saveButton.prop(_disabledProperty, true);
        } else {
            _saveButton.prop(_disabledProperty, false);
        }
    });


    _doUndoButton.click(function() {
        _taskView.trigger(Events.CHANGE_STATUS, _getTasksAttributes());
    });


    _deleteButton.click(function() {
        var _confirmation = confirm(_confirmDeletionMessage);
        if (_confirmation) {
            _taskView.trigger(Events.DELETE_TASK, {id: _self.id});
        }
    });


    _self.getTaskViewComponent = function() {
        return _taskView;
    };

    var _isBlankText = function(text) {
        return /^\s*$/.test(text);
    };

    var _getTasksAttributes = function(){
        var _tasksAttributes = {
            id: _self.id,
            tasksText: _textarea.val(),
            dateOfCreation: _self.dateOfCreation,
            creator: _creator,
            assignee: _assignee,
            tasksState: _self.tasksState
        }
        return _tasksAttributes;
    }

}