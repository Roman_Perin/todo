function ToDoList(eventBus, taskResolver, title){
    this.toDoListController = new ToDoListController(eventBus, title);
    this.taskService = new TaskService(eventBus, taskResolver);
    this.taskService.initWithTestData();
};