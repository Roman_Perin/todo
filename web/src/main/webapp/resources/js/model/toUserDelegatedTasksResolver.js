function ToUserDelegatedTasksResolver(){

    var _self = this;

    _self.getTasks = function(currentUser, callback){
        return $.ajax({
            url: '/tasks/delegatedtouser/' + currentUser.id,
            dataType: 'json',
            success: function (jsonTasks) {
                callback(jsonTasks);
//                def.resolve();
            }
        });
    };

}