function ByUserDelegatedTasksResolver(){

    var _self = this;

    _self.getTasks = function(currentUser, callback){
//    _self.getTasks = function(currentUser){
        return $.ajax({
            url:  '/tasks/delegatedbyuser/' + currentUser.id,
            dataType: 'json',
            success: function (jsonTasks) {
               callback(jsonTasks);
//               def.resolve();
            }
        });
    };

}