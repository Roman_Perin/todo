function TaskService(eventBus, taskResolver){
    var _self = this;
    var _tasksManager = new RemoteTasksManager(taskResolver);

    var _currentUser;


    _self.initWithTestData = function() {
        $.when(_tasksManager.initWithTestData()).done(function(tasks, currentUser){
            _currentUser = currentUser;
            _throwUpdateEvent(tasks);
        });
    };


    _self.addTask = function(tasksAttributes){
        $.when(_tasksManager.createTask(tasksAttributes)).done(function(){
            $.when(_tasksManager.getTasks(_currentUser)).done(function(tasks){
                _throwUpdateEvent(tasks);
            });
        });
    };

    _self.deleteTaskById = function(taskId){
        $.when(_tasksManager.deleteTaskById(taskId)).done(function(){
            $.when(_tasksManager.getTasks(_currentUser)).done(function(tasks){
                _throwUpdateEvent(tasks);
            });
        });
    };

    _self.updateTask = function(tasksAttributes){
        $.when(_tasksManager.updateTask(tasksAttributes)).done(function(){
            $.when(_tasksManager.getTasks(_currentUser)).done(function(tasks){
                _throwUpdateEvent(tasks);
            });
        });
    };


    eventBus.on(Events.ADD_TASK_CONTROLLER_EVENT, function(eventName, tasksDataHolder){
        tasksDataHolder.creator = _currentUser;
        var _assigneesLogin = tasksDataHolder.assigneesLogin;
        if (_assigneesLogin) {
            $.when(_tasksManager.getUserByLogin(_assigneesLogin)).done(function(assignee){
                tasksDataHolder.assignee = assignee;
                _self.addTask(tasksDataHolder);
            });
        } else {
            tasksDataHolder.assignee = _currentUser;
            _self.addTask(tasksDataHolder);
        }
    });

    eventBus.on(Events.CHANGE_STATUS_CONTROLLER_EVENT, function(eventName, tasksAttributes){
        var newState = _changeState(tasksAttributes.tasksState);
        tasksAttributes.tasksState = newState;
        _self.updateTask(tasksAttributes);
    });

    eventBus.on(Events.CHANGE_TEXT_CONTROLLER_EVENT, function(eventName, tasksAttributes){
        _self.updateTask(tasksAttributes);
    });

    eventBus.on(Events.DELETE_TASK_CONTROLLER_EVENT, function(eventName, initiator){
        _self.deleteTaskById(initiator.id);
    });


    var _changeState = function(oldState){
        var newState = (oldState === States.ACTIVE) ? States.EXECUTED: States.ACTIVE;
        return newState;
    };


    var _throwUpdateEvent = function(tasks){
        var _tasks = tasks;
        if (!_tasks){
            $.when(_tasksManager.getTasks(_currentUser)).done(function(tasks){
                _tasks = tasks;
            });
        }
        _sortTasksByDate(tasks);
        eventBus.trigger(Events.UPDATE_TASK_MODEL_EVENT, {tasks: _tasks, currentUser: _currentUser});
    };


    var _sortTasksByDate =  function(tasks) {
        tasks.sort(function compareAge(task1, task2) {
            return task2.dateOfCreation - task1.dateOfCreation;
        });
    };

}
