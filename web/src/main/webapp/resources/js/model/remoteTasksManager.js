function RemoteTasksManager(taskResolver) {
    var _self = this;
    var _tasks = [];


    _self.getTasks = function (currentUser) {
        var deferred = $.Deferred();
        taskResolver.getTasks(currentUser, function(jsonTasks) {
            _tasks.length = 0;
alert("manager: json.length: - " + jsonTasks.length);
            for (var task in jsonTasks) {
                if (jsonTasks[task]) {
                    $.when(_convertTaskDtoToTask(jsonTasks[task])).done(function (newTask) {
                        _tasks.push(newTask);
                        if (jsonTasks.length === _tasks.length) {
                            deferred.resolve(_tasks);
                        }
                    });
                }
            }
//            if (!jsonTasks.length) {
//               deferred.resolve(_tasks);
//            }
            if (jsonTasks.length === 0) {
                deferred.resolve(_tasks);
            }
        });
        return deferred.promise();
    };


    _self.initWithTestData = function () {
        var deferred = $.Deferred();
        $.ajax({
            url: '/users/current',
            dataType: 'json',
            success: function (currentUser) {
                $.when(_self.getTasks(currentUser)).done(function(tasks){
                    deferred.resolve(tasks, currentUser);
                });
            }
        });
        return deferred.promise();
    };


    _self.createTask = function (inputAttributes) {
        var _taskDto = {
            creatorsId: inputAttributes.creator.id,
            assigneesId: inputAttributes.assignee.id,
            tasksText: inputAttributes.tasksText
        };
        return $.ajax({
            url: '/tasks/create',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(_taskDto),
            contentType: "application/json"
        });
    };


    _self.deleteTaskById = function (taskId) {
        var deferred = $.Deferred();
        $.ajax({
            url: '/tasks/' + taskId + '/delete',
            type: 'DELETE',
            success: function(){
                deferred.resolve();
            }
        });
        return deferred.promise();
    };


    _self.updateTask = function (tasksAttributes) {
        var _taskDto = {
            id: tasksAttributes.id,
            tasksText: tasksAttributes.tasksText,
            tasksState: tasksAttributes.tasksState,
            assigneesId: tasksAttributes.assignee.id
        };
        return $.ajax({
            url: '/tasks/' + tasksAttributes.id + '/update',
            type: 'PUT',
            dataType: 'json',
            data: JSON.stringify(_taskDto),
            contentType: "application/json"
        });
    };


    _self.getUserByLogin = function(login){
        var deferred = $.Deferred();
        $.ajax({
            url: '/users/login/' + login,
            dataType: 'json',
            success: function (user) {
alert("manger get user with id: " + user.id)
                deferred.resolve(user);
            }
        });
        return deferred.promise();
    };


    var _getUserById = function (id) {
        var deferred = $.Deferred();
        $.ajax({
            url: '/users/' + id,
            dataType: 'json',
            success: function (user) {
                deferred.resolve(user);
            }
        });
        return deferred.promise();
    };


    var _convertTaskDtoToTask = function (taskDto) {
        var deferred = $.Deferred();
        $.when(_getUserById(taskDto.creatorsId), _getUserById(taskDto.assigneesId))
            .done(function(creator, assignee){
                var _creator = creator;
                var _assignee = assignee;
//                alert("assignee.id: " + _assignee.id);
//                alert("creator.id: " + _creator.id);
            var _tasksAttributes = {
                id: taskDto.id,
                tasksState: taskDto.tasksState,
                creator: _creator,
                assignee: _assignee,
                tasksText: taskDto.tasksText,
                dateOfCreation: new Date(taskDto.dateOfCreation)
            };
            deferred.resolve(new Task(_tasksAttributes));
        });
        return deferred.promise();
    }

}