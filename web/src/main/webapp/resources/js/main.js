
//todo: Is it norm that I have used just one 'require()' function and any 'define()'?

require (["ToDoList" , "States", "Events", "controller/toDoListController",
          "model/taskService", "model/task", "model/remoteTasksManager", "model/user",
          "model/byUserDelegatedTasksResolver", "model/toUserDelegatedTasksResolver",
          "view/TaskListCreatorView", "view/TaskView", "handlebars-v1.3.0"], function(){
    var todoListHolder1 = new ToDoList($("#todolist1"), new ToUserDelegatedTasksResolver(), "Tasks For Me");
    var todoListHolder2 = new ToDoList($("#todolist2"), new ByUserDelegatedTasksResolver(), "Tasks Delegated To Others");
});